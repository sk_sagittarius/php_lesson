<?php
function db_connect($database, $port = 3306)
{
    $connection = mysqli_connect('localhost', 'root', '', $database);
    if(mysqli_connect_errno())
        die(mysqli_connect_error());

    return $connection;

}

function db_checkOrDie($connection)
{
    if(!$connection || mysqli_errno($connection)) // проверка на то, есть ли ошибка
        die(mysqli_error($connection));
}

function db_getConnectionFromTable($table)
{
    $table = explode('.', $table);
    return db_connect($table[0] ?? '');
}

function db_getTable($table)
{
    return explode('.', $table)[1] ?? '';
}

function db_escapeData($data, $connection)
{
    if(!is_bool($data) && !is_null($data))
        $data = mysqli_real_escape_string($connection, $data); // защита от инъекций?
    if(is_numeric($data))
        return $data;
    if(is_bool($data))
        return $data ? 1 : 0;
    if(is_null($data))
        return 'null';
    if(is_string($data))
        return "'$data'";

     die("Incorrect \$data -> $data");
}

function db_select($table, $cols = "*", array $where = [])
{
    if(is_array($cols))
    {
        $cols = implode(',', $cols);
    }

    // разделяем на базу и таблицу (передаем через база.таблица)
    $connection = db_getConnectionFromTable($table);
    $table = db_getTable($table);
    $query = "select $cols from `$table`";

    if(count($where)>0)
    {
        $query .= " where";
        foreach($where as $col => $value)
        {
            $query .= " $col=" . db_escapeData($value, $connection);
        }
    }


    $result = mysqli_query($connection, $query);
    // превращаем в ассоциативный массив
    db_checkOrDie($connection);
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_close($connection);
    return $rows;
}

function db_insert($table, array $data)
{
    $connection = db_getConnectionFromTable($table);
    $table = db_getTable($table);

    $cols = array_keys($data);
    $cols = implode('.', $cols); // склеиваем

    // перезаписываем и возвращаем values
    $values = array_map(function ($item) use($connection){ // use - для использования глобальной переменной
        return db_escapeData($item, $connection);
    }, $data);

    $values = implode(',', $values);

    $query = "INSERT into $table ($cols) values ($values)";
    $result = mysqli_query($connection, $query);

    if($result == false)
        db_checkOrDie($connection);

    mysqli_close($connection);
    return $result;
}

function db_update($table, array $where, array $data)
{
    $connection = db_getConnectionFromTable($table);
    $table = db_getTable($table);

    $cols = array_keys($where);
    $cols = implode('.', $cols); // склеиваем
    $values = array_map(function ($item) use($connection){ // use - для использования глобальной переменной
        return db_escapeData($item, $connection);
    }, $where);

    $values = implode(',', $values);

    $colsData = array_keys($data);
    $colsData = implode('.', $colsData); // склеиваем
    $valuesData = array_map(function ($item) use($connection){ // use - для использования глобальной переменной
        return db_escapeData($item, $connection);
    }, $data);

    $valuesData = implode(',', $valuesData);



    $query = "UPDATE $table SET $cols = $values WHERE $colsData = $valuesData";


    $result = mysqli_query($connection, $query);

    if($result == false)
        db_checkOrDie($connection);

    mysqli_close($connection);
    return $result;

}

function db_delete($table, array $where)
{
    $connection = db_getConnectionFromTable($table);
    $table = db_getTable($table);

    $cols = array_keys($where);
    $cols = implode('.', $cols); // склеиваем
    $values = array_map(function ($item) use($connection){ // use - для использования глобальной переменной
        return db_escapeData($item, $connection);
    }, $where);

    $values = implode(',', $values);

    $query = "DELETE FROM $table WHERE $cols = $values";
    $result = mysqli_query($connection, $query);

    if($result == false)
        db_checkOrDie($connection);

    mysqli_close($connection);
    return $result;
}