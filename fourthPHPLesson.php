<!--// ctrl+alt+s settings -> cli -> + path-->

<!--<h2>Hello,--><?//= " world"; ?><!--</h2>-->
<!--<h2>Hello,--><?php //echo " world"; ?><!--</h2>-->

<?php
$data = ['First', 'Second', 'Third', 'Fourth'];
$i = 0;
$flag = true;
$number = 5;
$compare = 1;
?>

<!--<ul>-->
<!--    --><?php //foreach ($data as $item) : ?>
<!--        <li>--><?//= $item; ?><!--</li>-->
<!--    --><?php //endforeach; ?>
<!--</ul>-->
<!---->
<!--<ol>-->
<!--    --><?php //while($i<10) : ?>
<!--    <li>--><?//= $i++ ?><!--</li>-->
<!--    --><?php //endwhile; ?>
<!---->
<!--    --><?php //do { ?><!-- --><?php //} while($i==10); ?>
<!--    -->
<!--    --><?php //switch($i): ?>
<!--        --><?php //case(1): ?>
<!--        qwe-->
<!--        --><?php //break;
//    endswitch; ?>
<!--</ol>-->
<!---->
<?php //if ($flag): ?>
<!--    <h1>Flag</h1>-->
<?php //else : ?>
<!--    <p>Not flag</p>-->
<?php //endif ?>

<?php //if ($number>$compare): ?>
<!--    More than --><?//= $compare ?>
<?php //elseif ($number <$compare): ?>
<!--    Less than --><?//= $compare ?>
<?php //else: ?>
<!--    Equal to --><?//= $compare ?>
<?php //endif; ?>

<?php
error_reporting(E_ALL);

// подключение стороннего файла
// include_once - больше не ввызовет, но нужно указать каждому вызову или последнему
//include "check_number.php";
//$compare = 7;
//include "check_number.php";
//
//require "check_number.php";
//$compare = 5;
//require "check_number.php";
//
//include "5.php"; // warning + warning
//require "5.php"; // warning + fatal error

include_once "helpers.php";

render('templates/message.php');
render('templates/message.php', ['title' => 'Hello', 'message' => 'Hello, my friend', 'additional' => 'Add']);

//$data = include "data.php"; // return из файла кладем в переменную
$posts = include "task.php";
?>

<!--<!doctype html>-->
<!--<html>-->
<!--<head>-->
<!--    <meta charset="UTF-8">-->
<!--    <meta name="viewport"-->
<!--          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
<!--    <meta http-equiv="X-UA-Compatible" content="ie=edge">-->
<!--    <title>Document</title>-->
<!--</head>-->
<!--<body>-->
<?php //foreach ($posts as $title => $content)
//    render('templates/post.php', ['title' => $title, 'content' => $content]);
//?>
<!--</body>-->
<!--</html>-->

<?php foreach ($posts as $title => $content)
    render('templates/task.php', ['title' => $title, 'content' => $content]);
?>