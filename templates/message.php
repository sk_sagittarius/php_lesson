<div>
    <h1><?= $title ?? 'No title' ?></h1>
    <p><?= $message ?? 'No message' ?></p>
    <small>
<!--        --><?//= isset($additional) ? $additional : ''?>
        <?= $additional ?? '' ?>
    </small>
</div>