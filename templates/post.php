<!--<div style="border: 1px solid cornflowerblue">-->
<!--    <h3 style="margin: 0">-->
<!--        --><?//= $title ?? '[no title]' ?>
<!--    </h3>-->
<!--    <p style="margin: 0">-->
<!--        --><?//= $content ?? '[no content]' ?>
<!--    </p>-->
<!--</div>-->

<div style="border: 1px solid cornflowerblue">
    <?php
        echo tag('h3', ['style' => 'margin: 0'], $title ?? '[no title]');
        echo tag('p', ['style' => 'margin: 0'], $content ?? '[no content]');
    ?>
</div>