<?php
//declare(strict_types=1); // строгая типизация

$str = "Hello, world!";
$strRu = "Привет, мир!";
//$arr = [1,2,3,4];
//$a = "o";
//$b = "3";

//$arr = explode("$", $str);
//$arr2 = implode("$");

// вариант с for
//for($i = 0; $i < strlen($str); $i++)
//{
//    if($str[$i] == $a)
//    {
//        $str[$i] = $b;
//    }
//}
//
//echo $str;


// вариант с  foreach + новые функции
//foreach(str_split($str) as $key => $char)
//{
//    if($char == $a)
//    {
//        $str[$key] = $b;
//    }
//}
//echo $str;


// replace (что, на что, где)
//$newStr = str_replace('!', '?', $str);
//echo $newStr;


// замена каждого символа на рандомный символ из той же строки
//$chars = str_split($str);
//for($i = 0; $i < strlen($str); $i++)
//{
//    //$str[rand(0, strlen($str))]=$str[rand(0, strlen($str))];
//    $str[$i]=$str[rand(0, strlen($str)-1)];
//}
//echo $str;


// в верхний регистр
//echo strtoupper($str) . "\n";
//echo strtolower($str);

// для вывода не латиницы
// echo mb_strtoupper($strRu);


// функции для работы с массивом
// array_shift - удаляет с начала и отдает как переменную
// array_pop - удаляет с конца и отдает как переменную
// array_unshift - добавляет в начало
// array_push or [] - добавляет в конец
// unset - удаление переменной
// array_values - возвращение значений массива без ключа
// array_keys - возвращение ключей массива без значений

// implode - (разделитель, массив)
// explode - (разделитель, строка)
// str_split - разделить строку посимвольно

//$arr = [1, 2, 3, 4];
//$n = 1;
//for($i = 0; $i < $n; $i++)
//{
//    $item = array_pop($arr);
//    array_unshift($arr, $item);
//}
//var_dump($arr);

// заполнить массив и удалить нечетные по значению
//$arr = [];
//for ($i = 0; $i < 50; $i++)
//{
//    $arr[] = rand(1, 100);
//}
//
//$count = count($arr);
//for($i = 0; $i < $count; $i++)
//    if($arr[$i]%2>0)
//        unset($arr[$i]);
////var_dump($arr);
//
//// массив значений без ключей
//$val = array_values($arr);
//$key = array_keys($arr);
//print_r($val);


// сортировка
//$arr = [];
//for ($i = 0; $i < 50; $i++)
//{
//    $arr[] = rand(1, 100);
//}
//sort($arr);
//print_r($arr);

//$arr = [
//    [1, 2, "name"],
//    [3, 4, 6, false]
//];
//
//$n = 1;
//for($i = 0; $i < $n; $i++)
//{
//    $item = array_pop($arr);
//    array_unshift($arr, $item);
//}
//print_r($arr);



///////////////////// 27.02.2020

//function foo(int $a, int $b)
//{
//    return $a + $b;
//}
//
//echo foo(1, 2);

// сдвиг элементов массива
//function shift(array $arr, bool $toRight = true)
//{
//    if($toRight)
//    {
//        $item = array_pop($arr);
//        array_unshift($arr, $item);
//    }
//    else
//    {
//        $item = array_shift($arr);
//        array_push($arr, $item);
//    }
//    return $arr;
//
//}
//print_r(shift($arr, false));
//
//// сдвиг заданное количество раз
//$n = 13%count($arr); // сброс полных кругов, так как вернет тот же массив
//for($i = 0; $i < 5; $i++)
//{
//    $arr = shift($arr);
//}
//
//function _array_pull(array &$arr, bool $toRight = true)
//{
//    $arr = shift($arr);
//}
//
//function greeting(string &$name){}
//
//$num = 3;
//greeting($num); // преобразует в строку и сохранит
//var_dump($num);
//
//function foo(): string
//{
//    return 3;
//}
//var_dump(foo());

// fibonacci
//function fib($index)
//{
//    return $index > 1 ? fib($index - 1) + fib($index - 2) : $index;
//}
//echo fib(9);


//photo
//function array_simplify(array $arr) : array
//{
//
//}
//
//$array = [1, [2, 3, 4, [5, 6], 7], 8];
//print_r(array_simplify($array));

// удаление неуникальных значений из массива
//function my_array_unique(array $arr) : array
//{
//    $co = count($arr);
//    for($i = 0; $i < $co; $i++)
//    {
//        for($j = $i+1; $j < $co; $j++)
//        {
//            if($arr[$i] == $arr[$j])
//            {
//                unset($arr[$j]);
//            }
//        }
//    }
//    return $arr;
//}
//
//$arr = [3, 5, 5, 7, 3, 4, 8];
//print_r(my_array_unique($arr));


// анонимная функция
//$name = function ()
//{
//  return "Some";
//};
//
//echo $name();


// создать переменную name со значением Bob
//$vars = [
//    'name' => 'Bob',
//    'age' => 20
//];
//foreach($vars as $key => $item)
//{
//    $$key = $item;
//}
//echo $name;


// вызывать переменную с именем как значение a, которая вызывает функцию с именем как значение name
//$name = 'my_array_unique';
//$a = 'name';
//print_r($$a([1,2,3])); // = $name([1,2,3]);




