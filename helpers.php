<?php

function render($template, array $variables = [])
{
    $template = str_replace('/', '\\', $template); // заменяем все слэши на обратные, защита от дурака
    $template = str_replace('\\', DIRECTORY_SEPARATOR, $template); // заменяем на системный разделитель
    $template = ltrim($template, DIRECTORY_SEPARATOR); // удалить первый слэш если есть
    $template = rtrim($template, DIRECTORY_SEPARATOR); // удалить последний слэш если есть
    $basePath = getcwd();
    $template = $basePath . DIRECTORY_SEPARATOR . $template;

    if(!file_exists($template) || !is_file($template)) // вернет True даже если директория + поэтому проверка на файл
        die("Template {$template} not found"); // если файл не создан

    if($variables) // пустой или нет
        foreach ($variables as $variable => $value)
            $$variable = $value; // создаем новую переменную name variable = value value

    include $template; // подключаем файл
}

function tag($name, array $attributes = [], $body = null, bool $selfClosing = false)
{
    // базовые самозакрывающиеся теги
    $selfClosed = ['area', 'base', 'br', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'source'. 'param', 'track'];

    $selfClosing = in_array($name, $selfClosed) || $selfClosing;

    $tag = "<{$name}";

    $fAttr = function ($attr, $value = null)
    {
        $result = "";
        if(is_string($attr))
            $result = $attr;

        if(is_string($attr) && $value)
            $result .= '=';

        if($value)
            $result .= "\"{$value}\"";

        return $result;
    };

    foreach($attributes as $attribute => $value)
    {
        $tag .= " " . $fAttr($attribute, $value);
    }

    return $tag . ($selfClosing ? " />" : ">{$body}</{$name}>"); // простой вариант того, что ниже

//    if($selfClosing)
//        return $tag . " />";
//    return $tag . ">{$body}</{$name}>";

}


function csvToArray(string $file, $delimiter = ','): array {

    $res = [
        "count" => 0,
        "headers" => [],
        "data" => []
    ];

    if (!file_exists($file) || !is_file($file))
        return  $res;

    $lines = file($file);
    if (0 == $count = count($lines))
        return  $res;

    $res['count'] = $count - 1;
    $removeEol = function ($line) {
        $line = ltrim($line, "\t\n\r\0\x0B");
        return rtrim($line, "\t\n\r\0\x0B");
    };

    $res['headers'] = explode($delimiter, $removeEol(array_shift($lines)));

    foreach ($lines as $i => $line) {

        $res['data'][$i] = [];
        $line = explode($delimiter, $removeEol($line));

        foreach ($res['headers'] as $index => $header) {
            $res['data'][$i][$header] = $line[$index] ?? null;
        }
    }

    return $res;
}

